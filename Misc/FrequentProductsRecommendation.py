#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  6 16:39:29 2019

@author: danyal
"""

import pandas as pd
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules

base_dir = "/home/danyal/Projects/CCP"
df_original = pd.read_csv(base_dir + '/DataDir/OnlineRetail.csv')
xls_df = pd.read_excel(base_dir + '/DataDir/CloudServices.xlsx')
df_original.head()


#%%
topSellers = df_original.groupby(['Description'])['Description'].count().nlargest(300)
df = df_original[df_original.Description.isin(topSellers.index)]

xls_df['toReplace'] = df.Description.unique()
df['Description'] = df['Description'].map(xls_df.set_index('toReplace')['Services'])

df['Description'] = df['Description'].str.strip()
df.dropna(axis=0, subset=['InvoiceNo'], inplace=True)
df['InvoiceNo'] = df['InvoiceNo'].astype('str')
df = df[~df['InvoiceNo'].str.contains('C')]

#%%
basket = (df.groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo'))

def encode_units(x):
    if x <= 0:
        return 0
    if x >= 1:
        return 1

basket_sets = basket.applymap(encode_units)
#basket_sets.drop('POSTAGE', inplace=True, axis=1)

frequent_itemsets = apriori(basket_sets, min_support=0.03, use_colnames=True)

rules = association_rules(frequent_itemsets, metric="lift", min_threshold=0.4)
rules[['antecedents','consequents']].head(10)