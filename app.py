import pandas as pd
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
import json
import os
from flask import Flask,jsonify,request,render_template
#base_dir = "/home/danyal/Projects/CCP"
#df_original = pd.read_csv(base_dir + '/OnlineRetail.csv')

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
Data_FOLDER = os.path.join('DataDir')

app = Flask(__name__)
app.config['Data_FOLDER'] = Data_FOLDER;

static_folder = os.path.join('static')
app.config['static_folder'] = static_folder

full_filename = os.path.join(app.config['Data_FOLDER'], 'OnlineRetail.csv')    
df_original = pd.read_csv(full_filename)

services_filename = os.path.join(app.config['Data_FOLDER'], 'CloudServices.xlsx')    
xls_df = pd.read_excel(services_filename)

def encode_units(x):
    if x <= 0:
        return 0
    if x >= 1:
        return 1
    
@app.route('/')
def index():
    
    topSellers = df_original.groupby(['Description'])['Description'].count().nlargest(300)
    df = df_original[df_original.Description.isin(topSellers.index)]
    
    xls_df['toReplace'] = df.Description.unique()
    df['Description'] = df['Description'].map(xls_df.set_index('toReplace')['Services'])

    df['Description'] = df['Description'].str.strip()
    df.dropna(axis=0, subset=['InvoiceNo'], inplace=True)
    df['InvoiceNo'] = df['InvoiceNo'].astype('str')
    df = df[~df['InvoiceNo'].str.contains('C')]
    basket = (df.groupby(['InvoiceNo', 'Description'])['Quantity']
              .sum().unstack().reset_index().fillna(0)
              .set_index('InvoiceNo'))
    basket_sets = basket.applymap(encode_units)
    
    frequent_itemsets = apriori(basket_sets, min_support=0.03, use_colnames=True)
    
    rules = association_rules(frequent_itemsets, metric="lift", min_threshold=1)
    #rules['Product Use -> Recommended to Use'] = rules.antecedents.astype(str).str.cat(rules.consequents.astype(str), sep=' -> ')
    rules.rename(columns = {'antecedents' : 'Uses','consequents':'Recommends','lift':'Chance % Increase'},inplace = True)
    return render_template("show.html",result = rules[['Uses','Recommends','Chance % Increase']].sort_values(by='Chance % Increase' ,ascending=False))
    #return jsonify({'Best Guess Routes': rules[['antecedents','consequents','lift']].head(15).to_json(orient='index')})    

if __name__ == '__main__':
    #app.run(debug=True)
    import os
    if 'WINGDB_ACTIVE' in os.environ:
        app.debug = False    
    app.run()    